'use strict';

const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'))
const tablename = 'records';
const dbConnection = require('../../lib/mongodb');

exports.payload = Joi.object({
    startDate: Joi.date().required().format('YYYY-MM-DD').options({ convert: true }).description('startDate YYYY-mm-dd').error(new Error('startDate is missing')),
    endDate: Joi.date().required().format('YYYY-MM-DD').options({ convert: true }).description('endDate YYYY-mm-dd').error(new Error('endDate is missing')),
    minCount: Joi.number().required().integer().default(0).description('minCount should be integer').error(new Error('minCount is missing')),
    maxCount: Joi.number().required().integer().default(0).description('maxCount should be integer').error(new Error('maxCount is missing'))
}).required();//payload of dispatcher get driver api

/**
 * @method dispatcher/drivers
 * @param {*} req 
 * @param {*} res 
 */
exports.handler = (req, res) => {
    // Get query parameters
    let startDate = req.payload.startDate;
    let endDate = req.payload.endDate;
    let minCount = req.payload.minCount;
    let maxCount = req.payload.maxCount;

    let condition = [
        {
            '$project': {
                '_id': 0,
                'key': 1,
                'createdAt': 1,
                'totalCount': { '$sum': "$counts" }
            }
        },
        {
            '$match': {
                'createdAt': {
                    '$gte': new Date(startDate),
                    '$lte': new Date(endDate)
                },
                'totalCount': { '$gte': minCount, '$lte': maxCount }
            },
        },
        {
            '$sort': {
                "totalCount": 1
            }
        }];

    let getirCaseStudy = () => {
        return new Promise((resolve, reject) => {
            dbConnection.get().db(process.env.MONGO_DB).collection(tablename).aggregate(condition).toArray()
                .then(res => {
                    return resolve(res);
                }).catch(err => {
                    return reject(err);
                })
        });
    };
    return getirCaseStudy()
        .then((results) => {
            return res.response({
                code: 0,
                msg: 'Success',
                records: results
            }).code(200);
        }).catch((err) => {
            return res.response({
                code: 500,
                msg: err.message
            }).code(500);
        });
};