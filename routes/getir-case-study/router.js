'use strict';

const entity = '/api';
const getirCaseStudy = require('./getirCaseStudy');

module.exports = [
  /**
   * @name POST /api/getirCaseStudy
   */
  {
    method: 'POST',
    path: entity + '/getirCaseStudy',
    handler: getirCaseStudy.handler,
    config: {
      tags: ['api'],
      description: "This API filters all of the documents with startDate, endDate, minCount, maxCount",
      notes: "This API is to get all of the filtered documents.",
      auth: false,
      validate: {
        payload : getirCaseStudy.payload,
        failAction: (req, res, err) => {
          throw err;
        }
      }
    }
  }
];