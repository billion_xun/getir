'use strict'

const logger = require('winston');
const MongoClient = require('mongodb').MongoClient;
const url = require('./config');

let dbConnection = null;

/**
 * Method to connect to the mongodb
 * @param {*} url
 * @returns connection object
 */

exports.connect = async (callback) => {
    // if (dbConnection) return callback();
    // MongoClient.connect(url, function (err, connection) {
    //     if (err) {
    //         console.log(err)
    //         console.log(`MongoDB error connecting to ${url}`, err.message);
    //         process.exit(0);
    //     }
    //     dbConnection = connection; //assign the DB connection
    //     console.log(`MongoDB connected successfully -------------`);
    //     return callback();
    // });
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, connection) {
            if (err) {
                console.log(err)
                console.log(`MongoDB error connecting to ${url}`, err.message);
                process.exit(0);
            }
            dbConnection = connection; //assign the DB connection
            console.log(`MongoDB connected successfully -------------`);
            return resolve();
        });
    });

}

/**
 * Method to get the connection object of the mongodb
 * @returns dbConnection object
 */
exports.get = () => { return dbConnection }

/**
 * Method to close the mongodb connection
 */
exports.close = async (callback) => {
    if (dbConnection) {
        dbConnection.close((err, result) => {
            dbConnection = null;
            return callback(err);
        })
    }
}