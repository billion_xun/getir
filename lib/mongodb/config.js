'use strict'

const Joi = require('@hapi/joi')
const envVarsSchema = Joi.object({
    MONGO_URL: Joi.string().required()
}).unknown().required()

const envVars = Joi.attempt(process.env, envVarsSchema)

module.exports = envVars.MONGO_URL;
