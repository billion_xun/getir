'use strict'

require("dotenv").config();
const db = require('./mongodb');
const Hapi = require('@hapi/hapi');

// Create server listening on port
const server = Hapi.server({
    port: process.env.PORT,
    host: '127.0.0.1',
});

server.route(require('../routes')); //import the routes

server.route({
    method: 'GET',
    path: '/api/test',
    handler: (req, res) => {
        return res.response({
            msg: "Exists"
        }).code(200);
    }
});
// Initiate the server
exports.init = async () => {
    await server.start();
    console.log(`Server is listening on port ${server.info.uri}`);
    await db.connect(() => { }); //create a connection to mongodb
    return server;
};

// Stop the server
exports.stop = async () => {
    await server.stop();
    await db.close(() => { }); //create a connection to mongodb
};


process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});