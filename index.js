'use strict'

require("dotenv").config();
const db = require('./lib/mongodb');
const Hapi = require('@hapi/hapi');

// Create server listening on port
const server = Hapi.server({
    port: process.env.PORT || 5000,
    host: '0.0.0.0',
});

server.route(require('./routes')); //import the routes

server.route({
    method: 'POST',
    path: '/api/test',
    handler: (req, res) => {
        return res.response({
            msg: "Exists"
        }).code(200);
    }
});
// Initiate the server
const init = async () => {
    await server.start();
    console.log(`Server is listening on port ${server.info.uri}`);
    await db.connect(() => { }); //create a connection to mongodb
};


process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();

module.exports = server;