# Rest API **Getir** for Node.js (Simple & Small)

This repository is a getir for a Rest API with Node.js.

## Endpoints 
1. Get the filtered getir documents
  - URL: https://getir-deploy.herokuapp.com/api/getirCaseStudy
  - Content-Type: application/json
  - Method: POST
  - payload
  
    `
      {
          "startDate": "2016-01-02",
          "endDate": "2018-02-02",
          "minCount": 200,
          "maxCount": 1000
      }
	`