const server = require('../lib/server'); // Import Server/Application

//GET all the filtered documents      
describe("GET the filtered documents", () => {
    let AppServer;
    jest.setTimeout(30 * 1000);
    test('should success with 200', async () => {
        const options = {
            method: 'POST',
            url: '/api/getirCaseStudy',
            payload: JSON.stringify({
                startDate: "2016-01-02",
                endDate: "2018-02-02",
                minCount: 200,
                maxCount: 1000
            })
        };
        AppServer = await server.init();
        jest.setTimeout(30 * 1000);
        return new Promise((resolve, reject) => {
            AppServer.inject(options).then(data => {
                expect(data.statusCode).toBe(200);
                return resolve();
            }).catch(err => {
                return reject(err);
            })
        });
    });
});